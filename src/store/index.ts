import { set } from '@/utils/vuex';

export const state = () => ({
  header: false,
  footer: false,
  msg: {
    bool: false,
    color: '',
    message: '',
  },
  search: '',
  searchFocus: false,
});

export const mutations = {
  setHeader: set('header'),
  setFooter: set('footer'),
  setMsg: set('msg'),
  setBoolMsg: set('msg.bool'),
  setSearch: set('search'),
  setSearchFocus: set('searchFocus'),
};
