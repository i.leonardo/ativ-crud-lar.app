import { Component, Vue } from 'nuxt-property-decorator';

@Component({
  computed: {
    v() {
      return {
        required: (v: string) => !!v || 'Campo obrigatório',
      };
    },
  },
})
export default class Rules extends Vue {}
